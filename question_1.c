
#include <stdio.h>
typedef int bool;
#define true 1
#define false 0
int main()
{
	int i,j,k,n,num;
	
	printf("INPUT THE SIZE OF THE ARRAY:  ");//defines the size of the array
	scanf("%d", &n);
	int arr[n];
		
	//obtains the integers of the array upto n.
	for (i=0;i<n;i++){
				scanf("%d", &num);
				arr[i] = num;		
	}

	//prints out the input
	printf("The array is: { ");
	for (i=0;i<n;i++){
			printf("%d", arr[i]);
			printf(" ");
	}
	printf("}\n");
	
	//prints out the triplets
	bool found;
	for (i=0;i<n-2;i++)
	{
		for (j=i+1;j<n-1;j++)
		{
			for (k=j+1;k<n;k++)
			{
				if ((arr[i]+arr[j]+arr[k])==0)
				{
					printf("%d %d %d\n", arr[i], arr[j], arr[k]);
					found =true;
				}
				
			}
		}
	}
	
	if (found == false){
		printf("Triples do not exist here");
	}	
	
}

